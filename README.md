# ova2libvirt

This is a simple shell script that automates the process of import the virtual machine in OVA file to libvirt guest format in local machine. 

## Requirements

To use the ova2libvirt you need to have installed the following tools:

* `qemu-img`
* `sudo`
* `tar`
* `virsh`
* `virt-convert`

## Basic utilization

`ova2libvirt -f FILE.ova [OPTIONS]`

Convert FILE.ova to native libvirt guests format. If -i option is seted, then import virtual machine in URI qemu:///system

**OPTIONS**

`-c URI`            Connect to a non-default hypervisor. Default URI is qemu:///system

`-i`                Import VM after extraction. Default is no

`-f FILE.ova`       OVA file that will be converted

`-o [OUTPUT-DIR]`   Default is create the directory FILE.ova_output

 ova2libvirt version 0.1

## License

ova2libvirt is licensed under GPL-3+.
