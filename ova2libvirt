#!/bin/bash
# This script import OVA machines to QEMU-KVM
#  - The basic work of program is receveid the ova file and import this.
#  TODO: Create option to export QEMU virtualmachine to OVA;
#
#  SYNOPSE:
#          ova2libvirt -f FILE.ova -i
#
# REQUIRE: virt-convert qemu-img virsh tar
#
# Autor: Marcio de Souza Oliveira <marciosouza@debian.org>
# Licensed by GPL-3.
set -e

# Global variables
VERSION='0.1'
REQUIRE='virt-convert qemu-img virsh tar sudo'
USE="Usage: $(basename $0) -f FILE.ova [OPTIONS]"

# Default options
IMPORT="False"
URI="qemu:///system"
OUTPUT_DIR=""
FILE=""
POOL="default"
ORIGIN_DISK=""
OUTPUT_DISK=""

display_help () {
	echo -e "$USE\n"
	echo "Convert FILE.ova to native libvirt guests format. If -i option is seted, then import virtual machine in URI qemu:///system"
	echo
	echo "OPTIONS"
	echo "-c URI		Connect to a non-default hypervisor. Default URI is qemu:///system"
	echo "-i  		Import VM after extraction. Default is no"
	echo "-f FILE.ova  	OVA file that will be converted"
	echo "-o [OUTPUT-DIR]	Default is create the directory FILE.ova_output"
	echo -e "\n $(basename "$0") version "$VERSION""

}


check_requirements () {
	for i in $REQUIRE
	do
		which "$i" >& /dev/null || (echo "[*] ERROR: "$i" not found" &&  echo "[**] Please install requirement: "$i"" && return 2)
	done
}

check_requirements || exit 2

if [ "$#" != 0 ]
then
	while true
	do
		case "$1" in
			-h|--help)
				display_help && exit 0
				;;
			-c|--connect)
				URI="$2"
				shift 2
				;;
			-e)
				echo "Not implemented yet!" && exit 0
				;;
			-f|--file)
				FILE="$2"
				shift 2
				;;
			-i|--import)
				IMPORT="True"
				shift 1
				;;
			-o|--output)
				OUTPUT_DIR="$2"
				shift 2
				;;
			-*)
				echo "$USE" && exit 1
				;;
			*)
				if [ "$#" = 0 ]
				then
					break
				else
					echo "$USE" && exit 3
				fi
				;;
		esac
	done
else
	echo "$USE" && exit 2
fi

get_the_default_pool () {
	#TODO: implement the select statement to allow the user choose the pool
	#      check avaliable space in destination pool
	echo "[*] Getting the pool-list..."
	virsh -c "$URI" pool-list
}


check_input_file () {
# Check the permissions and create output directory
	if [ -r "$FILE" ]
	then
		if [ -z "$OUTPUT_DIR" ]; then
			export OUTPUT_DIR=""$FILE"_output"
			echo "[*] Creating the output directory: "$OUTPUT_DIR""
			mkdir "$OUTPUT_DIR" || (echo "[!] Error! Cannot create the output directory" && return 4)
		else
			echo "[*] Creating the output directory: "$OUTPUT_DIR""
			mkdir "$OUTPUT_DIR" || (echo "[!] Error! Cannot create the output directory" && return 4)
		fi
	else
		echo "[!] Error cannot access the ova file: "$FILE"" && return 5
	fi
}

extract_ova_file () {
# Extract the OVA file to OUTPUT dir
	echo -e "[*] Extracting OVA file to $OUTPUT_DIR:\n"
	tar -xvf "$FILE" -C "$OUTPUT_DIR" || (echo -e "[!] Error! Cannot extract OVA file: "$FILE"\n[!] Check the file, may be corrupt!" && return 6)
}


convert_disk_file () {
	# Try identify disk file (VMDK|VDI) and converts it to qcow2
	echo [*] Identifying the disk file
	vmdk_disks=$(ls "$OUTPUT_DIR"/*.vmdk)
	qtd_vmdk_disks=$(ls "$OUTPUT_DIR"/*.vmdk | wc -l)
#	vdi_disks=$(ls "$OUTPUT_DIR"/*.vdi)
#	qtd_vdi_disks=$(ls "$OUTPUT_DIR"/*.vdi | wc -l)
#	qtd_vdi=0
	if [ "$qtd_vmdk_disks" = 1 ]
	then
		echo "[*] "$0" found one vmdk disk: "$vmdk_disks""
		echo -e "[*] Checking information about the disk file:\n" 
		qemu-img info "$vmdk_disks" || ( echo "[!] Error! Cannot dump information about disk file, may be corrupted!" && return 7)
	#	echo "Do you wish proceed with conversion to qcow2?"
	#	read answer
	#	[ "$answer" != S ] && ( echo -e "[!] Aborting by user choice!\n[!] Please remove the file in $(ls -l $OUTPUT_DIR" && exit 8)"
		output_disk_qcow2=$(echo "$vmdk_disks" | sed 's/\.vmdk/\.qcow2/g')
		echo -e "[*] Converting disk "$vmdk_disks" to "$output_disk_qcow2":\n"
		qemu-img convert -c -p -O qcow2 "$vmdk_disks" "$output_disk_qcow2" || ( echo "[!] Error! Cannot convert disk file to qcow2!" && return 9)
	else
		echo "[!] Error! Not implemented yet!" && exit 10
	fi

}

import_vm_from_ovf () {
	# Convert ovf file to xml format
	cd "$OUTPUT_DIR"
	ovf_file=$(ls *.ovf)
	if [ -r "$ovf_file" ]; then
		xml_file=$(echo "$ovf_file" | sed 's/\.ovf/\.xml/g')
		echo -e "[*] Converting "$ovf_file" to $xml_file\n[*] Converting disk... and importing VM in default "$URI""
		sudo virt-convert --connect "$URI" -d "$ovf_file" --disk-format qcow2 || (echo "[!] Error! Cannot convert and import VM" && return 10)
	else
		echo "[!] Error! Cannot read ovf file!" && return 11

	fi

}

check_input_file && extract_ova_file
[ "$IMPORT" = "True" ] && import_vm_from_ovf
